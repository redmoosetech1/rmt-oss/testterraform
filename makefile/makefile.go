package makefile

import (
	"math/rand"
	"os"
	"strconv"
)

type MFile struct {
	ID       string `json:"id"`
	FileName string `json:"filename"`
}

func New() *Data {
	fp, err := os.Create(JsonFile)
	if err != nil {
		return nil
	}
	defer fp.Close()

	_, err = fp.WriteString("{}")
	if err != nil {
		return nil
	}

	return &Data{
		jfile: JsonFile,
	}
}

func (d *Data) Create(fn string, c string) (string, error) {

	id := strconv.Itoa(rand.Intn(100))
	for ok := true; ok; ok = !d.IdIsUnique(id) {
		id = strconv.Itoa(rand.Intn(100))
	}

	mf := MFile{
		ID:       id,
		FileName: fn,
	}

	file, err := os.Create(fn)
	if err != nil {
		return "", err
	}

	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			return
		}
	}(file)

	_, err = file.WriteString(c)
	if err != nil {
		return "", err
	}
	err = d.Put(mf)
	if err != nil {
		return "", err
	}

	return mf.ID, nil
}

func (d *Data) Read(id string) (string, error) {
	mf, err := d.Get(id)
	if err != nil {
		return "", err
	}

	data, err := os.ReadFile(mf.FileName)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

func (d *Data) Update(id string, c string) error {
	fm, err := d.Get(id)
	if err != nil {
		return err
	}

	file, _ := os.Create(fm.FileName)
	defer func(file *os.File) {
		err := file.Close()
		if err != nil {
			return
		}
	}(file)

	_, err = file.WriteString(c)
	if err != nil {
		return err
	}
	return nil
}

func (d *Data) Delete(id string) error {
	fm, err := d.Get(id)
	if err != nil {
		return err
	}

	err = os.Remove(fm.FileName)
	if err != nil {
		return err
	}

	return nil
}
