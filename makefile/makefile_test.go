package makefile

import (
	"os"
	"testing"
)

func TestCreateRead(t *testing.T) {
	mf := New()
	id, err := mf.Create("/tmp/test.txt", "Hello, World!!!")
	if err != nil {
		t.Error("failed to create")
	}

	if _, err := os.Stat("/tmp/test.txt"); os.IsNotExist(err) {
		t.Error("file does not exist")
	}

	content, err := mf.Read(id)
	if err != nil {
		t.Error("error")
	}

	if content != "Hello, World!!!" {
		t.Error("error")
	}

	os.Remove("/tmp/test.txt")
	os.Remove("/tmp/test.json")
}

func TestUpdate(t *testing.T) {
	mf := New()
	id, err := mf.Create("/tmp/test.txt", "Hello, World!!!")
	if err != nil {
		t.Error("failed to create")
	}

	if _, err := os.Stat("/tmp/test.txt"); os.IsNotExist(err) {
		t.Error("file does not exist")
	}

	content, err := mf.Read(id)
	if err != nil {
		t.Error("error")
	}

	if content != "Hello, World!!!" {
		t.Error("error")
	}

	err = mf.Update(id, "Hello, Country!!!")
	if err != nil {
		t.Error("failed to update")
	}

	content, err = mf.Read(id)
	if err != nil {
		t.Error("error")
	}

	if content != "Hello, Country!!!" {
		t.Error("error")
	}

	os.Remove("/tmp/test.txt")
	os.Remove("/tmp/test.json")

}
