package makefile

import (
	"encoding/json"
	"errors"
	"io/ioutil"
)

const JsonFile string = "/tmp/test.json"

type Data struct {
	jfile string
}

func (d *Data) Get(id string) (MFile, error) {
	data, err := d.GetAll()
	if err != nil {
		return MFile{}, err
	}

	for _, s := range data {
		if s.ID == id {
			return s, nil
		}
	}

	return MFile{}, errors.New("Nothing by this ID ...")
}

func (d *Data) Put(fm MFile) error {

	data, err := d.GetAll()
	if err != nil {
		return err
	}

	data = append(data, fm)
	file, err := json.MarshalIndent(data, "", " ")
	if err != nil {
		return err
	}

	err = ioutil.WriteFile("/tmp/test.json", file, 0644)
	if err != nil {
		return err
	}

	return nil
}

func (d *Data) Remove(id string) error {
	data, err := d.GetAll()
	if err != nil {
		return err
	}

	var newData []MFile
	for i, s := range data {
		if s.ID == id {
			data[i] = data[len(data)-1]
			newData = data[:len(data)-1]
			break
		}
	}

	file, err := json.MarshalIndent(newData, "", " ")
	if err != nil {
		return err
	}

	err = ioutil.WriteFile(d.jfile, file, 0644)
	if err != nil {
		return err
	}

	return nil
}

func (d *Data) GetAll() ([]MFile, error) {
	data := make([]MFile, 0)
	content, err := ioutil.ReadFile(d.jfile)
	if err != nil {
		return nil, err
	} else if string(content) == "{}" {
		return data, nil
	}

	err = json.Unmarshal(content, &data)
	if err != nil {
		return nil, err
	}

	return data, nil
}

func (d *Data) IdIsUnique(id string) bool {
	data, err := d.GetAll()
	if err != nil {
		return true
	}

	for _, s := range data {
		if s.ID == id {
			return false
		}
	}
	return true
}
