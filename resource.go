package main

import (
	"context"
	"github.com/hashicorp/terraform-plugin-framework/path"
	"github.com/hashicorp/terraform-plugin-framework/resource"
	"github.com/hashicorp/terraform-plugin-framework/resource/schema"
	"github.com/hashicorp/terraform-plugin-framework/types"
	"github.com/hashicorp/terraform-plugin-log/tflog"
	"gitlab.com/redmoosetech1/rmt-oss/testterraform/makefile"
)

// Ensure the implementation satisfies the expected interfaces.
var (
	_ resource.Resource                = &makefileResource{}
	_ resource.ResourceWithConfigure   = &makefileResource{}
	_ resource.ResourceWithImportState = &makefileResource{}
)

type makefileResource struct {
	provider makefileProvider
	client   *makefile.FileModel
}

type makefileResourceModel struct {
	ID      types.String `tfsdk:"id"`
	content types.String `tfsdk:"content"`
}

type makefileResourceData struct {
	ConfigurableAttribute types.String `tfsdk:"configurable_attribute"`
	Id                    types.String `tfsdk:"id"`
}

func NewResource() resource.Resource {
	return &makefileResource{}
}

func (mfr *makefileResource) Metadata(_ context.Context, req resource.MetadataRequest, resp *resource.MetadataResponse) {
	resp.TypeName = req.ProviderTypeName + "_makefile"
}

func (mfr *makefileResource) Schema(_ context.Context, _ resource.SchemaRequest, resp *resource.SchemaResponse) {
	resp.Schema = schema.Schema{
		Description: "Make File",
		Attributes: map[string]schema.Attribute{
			"content": schema.StringAttribute{
				Optional: false,
			},
		},
	}
}

func (mfr *makefileResource) Configure(_ context.Context, req resource.ConfigureRequest, _ *resource.ConfigureResponse) {
	if req.ProviderData == nil {
		return
	}

	mfr.client = req.ProviderData.(*makefile.FileModel)
}

func (mfr *makefileResource) Create(ctx context.Context, req resource.CreateRequest, resp *resource.CreateResponse) {
	// Retrieve values from plan
	var plan makefileResourceModel
	diags := req.Plan.Get(ctx, &plan)
	resp.Diagnostics.Append(diags...)
	if resp.Diagnostics.HasError() {
		return
	}

	mfr.client.Create(plan.content.String())
	// Create resource using 3rd party API.

	plan.ID = types.StringValue(mfr.client.ID)
	tflog.Trace(ctx, "created a resource")

	diags = resp.State.Set(ctx, &plan)
	resp.Diagnostics.Append(diags...)
}

func (mfr *makefileResource) Read(ctx context.Context, req resource.ReadRequest, resp *resource.ReadResponse) {
	var data makefileResourceData

	diags := req.State.Get(ctx, &data)
	resp.Diagnostics.Append(diags...)
	if resp.Diagnostics.HasError() {
		return
	}

	// Get refreshed order value from MakeFile
	data := mfr.client.Read()

	// Read resource using 3rd party API.
	diags = resp.State.Set(ctx, &data)
	resp.Diagnostics.Append(diags...)
}

func (mfr *makefileResource) Update(ctx context.Context, req resource.UpdateRequest, resp *resource.UpdateResponse) {
	var data makefileResourceData

	diags := req.Plan.Get(ctx, &data)
	resp.Diagnostics.Append(diags...)

	if resp.Diagnostics.HasError() {
		return
	}

	// Update resource using 3rd party API.

	diags = resp.State.Set(ctx, &data)
	resp.Diagnostics.Append(diags...)
}

func (mfr *makefileResource) Delete(ctx context.Context, req resource.DeleteRequest, resp *resource.DeleteResponse) {
	var data makefileResourceData

	diags := req.State.Get(ctx, &data)
	resp.Diagnostics.Append(diags...)

	if resp.Diagnostics.HasError() {
		return
	}

	// Delete resource using 3rd party API.
}

func (mfr *makefileResource) ImportState(ctx context.Context, req resource.ImportStateRequest, resp *resource.ImportStateResponse) {
	// Retrieve import ID and save to id attribute
	resource.ImportStatePassthroughID(ctx, path.Root("id"), req, resp)
}
