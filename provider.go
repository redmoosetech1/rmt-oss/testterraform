package main

import (
	"context"
	"github.com/hashicorp/terraform-plugin-framework/datasource"
	"github.com/hashicorp/terraform-plugin-framework/path"
	"github.com/hashicorp/terraform-plugin-framework/provider"
	"github.com/hashicorp/terraform-plugin-framework/provider/schema"
	"github.com/hashicorp/terraform-plugin-framework/resource"
	"github.com/hashicorp/terraform-plugin-framework/types"
	"github.com/hashicorp/terraform-plugin-log/tflog"
	"gitlab.com/redmoosetech1/rmt-oss/testterraform/makefile"
)

var _ provider.Provider = &makefileProvider{}

type makefileProvider struct {
	version string
}

type makefileProviderModel struct {
	FileName types.String `tfsdk:"filename"`
}

func New() func() provider.Provider {
	return func() provider.Provider {
		return &makefileProvider{
			version: "1.0.0",
		}
	}
}

func (p *makefileProvider) Configure(ctx context.Context, req provider.ConfigureRequest, resp *provider.ConfigureResponse) {
	tflog.Info(ctx, "Configuring MakeFile")

	// Retrieve provider data from configuration
	var config makefileProviderModel
	diags := req.Config.Get(ctx, &config)
	resp.Diagnostics.Append(diags...)
	if resp.Diagnostics.HasError() {
		return
	}

	// If practitioner provided a configuration value for any of the
	// attributes, it must be a known value
	if config.FileName.IsUnknown() {
		resp.Diagnostics.AddAttributeError(path.Root("filename"),
			"Unknown FileName",
			"The provider cannot create file without filename",
		)
	}

	if resp.Diagnostics.HasError() {
		return
	}

	client := makefile.New(config.FileName.String())
	resp.ResourceData = client

	tflog.Info(ctx, "Configured MakeFile Client", map[string]any{"success": true})
}

func (p *makefileProvider) Metadata(ctx context.Context, req provider.MetadataRequest, resp *provider.MetadataResponse) {
	resp.TypeName = "example"
	resp.Version = p.version
}

func (p *makefileProvider) DataSources(ctx context.Context) []func() datasource.DataSource {
	return []func() datasource.DataSource{
		NewDataSource,
	}
}

func NewDataSource() datasource.DataSource {
	return nil
}

func (p *makefileProvider) Resources(ctx context.Context) []func() resource.Resource {
	return []func() resource.Resource{
		NewResource,
	}
}

func (p *makefileProvider) Schema(ctx context.Context, req provider.SchemaRequest, resp *provider.SchemaResponse) {
	resp.Schema = schema.Schema{
		Attributes: map[string]schema.Attribute{
			"filename": schema.StringAttribute{
				MarkdownDescription: "Name of file you want to write too",
				Optional:            false,
			},
		},
	}
}
